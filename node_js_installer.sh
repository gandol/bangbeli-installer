#!/bin/bash
apt update
curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
apt install -y nodejs
npm install -g yarn
npm install -g pm2
pm2 startup